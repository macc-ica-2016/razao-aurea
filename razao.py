'''
Created on 30/03/2016

@author: fabiano
'''
import numpy as np
import matplotlib.pyplot as plt


def secao_aurea(funcao,a,b,e):
    d = b-a
    if(e>0): 
        r =  (np.sqrt(5)-1)/2   
        alpha = a + (1-r)*d
        beta = a + r*d
        y1 = funcao(alpha)
        y2 = funcao(beta)
        i=0
        while(b-a)>e:
            i+=1
            if(y1>y2):
                a=alpha
                alpha = beta
                y1=y2
                beta=a+r*(b-a)
                y2=funcao(beta)
            else:
                b=beta
                beta=alpha
                y2=y1
                alpha =  a + (1-r)*(b-a)
                y1= funcao(alpha)
    alpha = (b+a)/2
    print("Qntde de iteracoes:"+str(i))
    return alpha

def fx1(x):
    return x**2-4*x+3

def fx2(x):
    return np.exp(-x)+x**2


def exercio_npc1(a,b,f,e):
    x = np.arange(a, b, (b-a)/10);
    y = fx1(x)
    print("X\tY")
    for (i,j) in zip(x, y):
        print(str(i)+"\t"+str(j))
    plt.plot(x, y)
    plt.savefig(str(f)+'.png')
    print("E: "+str(e))
    print("Minimo: "+str(secao_aurea(f, a, b, e)))
    

if __name__ == '__main__':
    exercio_npc1(0,3,fx1,0.1)
    exercio_npc1(0,3,fx1,0.01)
    
    exercio_npc1(-1,1,fx2,0.01)
    exercio_npc1(-1,1,fx2,0.001)
    #plt.show()
    
    
    